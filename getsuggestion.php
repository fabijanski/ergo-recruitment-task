<?php

$suggestions = array("John", "Jack", "Michal", "Piotr", "Salma", "Sandra", "Jacek", "Bart");

// get the user's input value
$q = $_GET["q"];

// check if there is any string value from the client's request 
if (strlen($q) > 0) {
    $hints = array();
    foreach ($suggestions as $name) {
        if ( strpos($name, $q) !== false ) {
            array_push($hints, $name);
        }
    }
}

// If there is no hint available return 'no hints'
if (empty($hints)) {
  $response="no hints";
// otherwise return an array of available hints
} else {
  $response=$hints;
}

//output the response
echo json_encode($response);

?>