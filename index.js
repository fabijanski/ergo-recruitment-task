function showSuggestions(value) {
  const suggestions = document.querySelector("#txtHint");
  if (value.length === 0) {
    // clearing the output for empty input value
    suggestions.innerHTML = "";
    return;
  } else {
    // removing any of existing suggestions before next searching
    suggestions.innerHTML = "";
    let names = [];
    const suggestionsPromise = fetch(`http://localhost:8080/getsuggestion.php?q=${value}`);

    // sending request to the server file
    // adding results to an empty array of names
    suggestionsPromise
      .then(data => data.json())
      .then(data => {
        data.map(name => names.push(name))
      });

    // rendering the results
    suggestions.innerHTML = names;
  }
}